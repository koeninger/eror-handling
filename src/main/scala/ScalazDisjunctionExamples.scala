object ScalazDisjunctionExamples {
  import scalaz._
  import Scalaz._

  def checkUser: \/[String, String] = \/-("a good user")
  def checkPass: \/[String, String] = -\/("a bad password")

  for {
    user <- checkUser
    password <- checkPass
  } yield s"authed $user $password"

}
