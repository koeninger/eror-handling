sealed trait MyEither[+E, +A] {
  def map[B](f: A => B): MyEither[E, B] =
    this match {
      case MyRight(a) => MyRight(f(a))
      case MyLeft(e) => MyLeft(e)
    }

  def flatMap[EE >: E, B](f: A => MyEither[EE, B]): MyEither[EE, B] =
    this match {
      case MyRight(a) => f(a)
      case MyLeft(e) => MyLeft(e)
    }

  def orElse[EE >: E,B >: A](b: => MyEither[EE, B]): MyEither[EE, B] =
    this match {
      case MyRight(a) => MyRight(a)
      case _ => b
    }

  def map2[EE >: E, B, C](b: MyEither[EE, B])(f: (A, B) => C): MyEither[EE, C] =
    for {
      x <- this
      y <- b
    } yield f(x, y)

}

case class MyLeft[+E](value: E) extends MyEither[E, Nothing]
case class MyRight[+A](value: A) extends MyEither[Nothing, A]

object MyEither {
  //ex 8
  // sequence & traverse
  def sequence[E, A](a: List[MyEither[E, A]]): MyEither[E, List[A]] =
    traverse(a)(x => x)

  def traverse[E, A, B](la: List[A])(f: A => MyEither[E, B]): MyEither[E, List[B]] =
    la.foldRight(MyRight[List[B]](Nil): MyEither[E, List[B]])((a, elb) =>
      f(a).map2(elb)(_ :: _))

}

object ProjectionsAreFun {

/* Which of these will work, what will they do 

  for {
    user <- Right("a good user")
    password <- Left("a bad pass")
  } yield s"authed $user $password"

  for {
    user <- MyRight("a good user")
    password <- MyLeft("a bad pass")
  } yield s"authed $user $password"

  for {
    user <- MyLeft("a bad user")
    password <- MyLeft("a bad pass")
  } yield s"authed $user $password"

  for {
    user <- Right("a good user").right
    password <- Left("a bad pass").right
  } yield s"authed $user $password"

  for {
    user <- Right("a good user").right
    password <- Left("a bad pass").left
  } yield s"authed $user $password"

 */

  /* you can use scalaz disjunction, or just do

  implicit def eitherToRight[A,B](e: Either[A, B]): Either.RightProjection[A, B] = e.right

   */
}
