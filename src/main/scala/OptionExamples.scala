sealed trait MyOption[+A] {
  def map[B](f: A => B): MyOption[B] =
    this match {
      case MySome(a) => MySome(f(a))
      case MyNone => MyNone
    }

  def getOrElse[B >: A](default: => B): B =
    this match {
      case MySome(a) => a
      case MyNone => default
    }

  def flatMap[B](f: A => MyOption[B]): MyOption[B] =
    this.map(f).getOrElse(MyNone)

  def orElse[B >: A](ob: => MyOption[B]): MyOption[B] =
    this.map(MySome(_)).getOrElse(ob)

  def filter(f: A => Boolean): MyOption[A] =
    this.flatMap(i => if (f(i)) MySome(i) else MyNone)
}

case class MySome[+A](get: A) extends MyOption[A]
case object MyNone extends MyOption[Nothing]

object MyOption {
  def map2[A, B, C](a: MyOption[A], b: MyOption[B])(f: (A, B) => C): MyOption[C] =
    for {
      x <- a
      y <- b
    } yield f(x, y)

  def sequence[A](loa: List[MyOption[A]]): MyOption[List[A]] =
    loa.foldLeft(MySome(Nil): MyOption[List[A]]) { (ola, oa) =>
      ola.flatMap(la => oa.map(_ :: la))
    }.map(_.reverse)

  def traverse[A, B](la: List[A])(f: A => MyOption[B]): MyOption[List[B]] =
    la.foldRight(MySome(Nil): MyOption[List[B]]) { (a, olb) =>
      olb.flatMap(lb => f(a).map(_ :: lb))
    }

  def sequenceByTraverse[A](loa: List[MyOption[A]]): MyOption[List[A]] =
    traverse(loa)(x => x)
}


object NullsAreFun {
  def greetUnsafe(name: Option[String]): String = {
    val n = name.getOrElse("unknown")
    s"hello ${n}, your name is ${n.length} characters long"
  }

  def greetSafe(name: Option[String]): String = {
    val n = name match {
      case Some(s: String) => s
      case _ => "unknown"
    }
    s"hello ${n}, your name is ${n.length} characters long"
  }
   

/* decompiled:

    public String greetUnsafe(Option<String> name) {
        String n = (String)name.getOrElse((Function0)new scala.Serializable(this){
            public static final long serialVersionUID = 0;

            public final String apply() {
                return "unknown";
 }
 });
        return new StringContext((Seq)Predef..MODULE$.wrapRefArray((Object[])new String[]{"hello ", ", your name is ", " characters long"})).s((Seq)Predef..MODULE$.genericWrapArray((Object)new Object[]{n, BoxesRunTime.boxToInteger((int)n.length())}));
 }

    public String greetSafe(Option<String> name) {
        String s;
        String string;
        Some some;
        Option<String> option = name;
        String string2 = option instanceof Some && (s = (String)(some = (Some)option).x()) != null ? (string = s) : "unknown";
        String n = string2;
        return new StringContext((Seq)Predef..MODULE$.wrapRefArray((Object[])new String[]{"hello ", ", your name is ", " characters long"})).s((Seq)Predef..MODULE$.genericWrapArray((Object)new Object[]{n, BoxesRunTime.boxToInteger((int)n.length())}));
 }

 */  


  // Option's apply method does null checks, Some does not
  def greetSafe(name: String): String = greetUnsafe(Option(name))
}


object SubtypingIsFun {
  // ok, which of these will work

/*

  val ex1 = for {
    a <- List(1, 2)
    b <- Some("bob")
  } yield s"$a $b"

  // which is equivalent to
  val desugarEx1 = List(1, 2).flatMap { a =>
    Some("bob").map { b =>
      s"$a $b" } }

  val ex2 = for {
    a <- List(1, 2)
    b <- MySome("me")
  } yield s"$a $b"

  val ex3 = for {
    a <- MySome("me")
    b <- MyNone
  } yield s"$a $b"

  val ex4 = for {
    a <- Some("bob")
    b <- List(1, 2)
  } yield s"$a $b"

  val ex5 = for {
    a <- Some("bob"): Iterable[_]
    b <- List(1, 2)
  } yield s"$a $b"


 */

/*  signatures of flatMap:

  def flatMap[B](f: (A) ⇒ Option[B]): Option[B] 

  def flatMap[B](f: (A) ⇒ GenTraversableOnce[B]): List[B] 

  Option's companion object contains:

  implicit def option2Iterable[A](xo: Option[A]): Iterable[A] = xo.toList

  Iterable is a subtype of GenTraversableOnce

 */
}
