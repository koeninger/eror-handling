object ScalazOptionExamples {
  import scalaz._
  import scalaz.syntax.traverse._
  import scalaz.std.AllInstances._
  import scalaz.std.AllFunctions._

  assert(some(1) == Some(1))
  val anOption = some(1).getClass
  val aSome = Some(1).getClass

  // there is no scalaz option() method that does null checks, just use Option.apply
  assert(some(1) == Option(1))
  val stillAnOption = Option(1).getClass

  // contradiction to FP in Scala opinion that sequence should only be on companion object
  assert(List(some(1), some(2)).sequence == Some(List(1, 2)))

  assert(List(Option(1), Option(2)).sequence == Some(List(1, 2)))

  assert(List(Option("bob"), Option(null)).sequence == None)

  // scalaz has Maybe (like Option w/o subclassing), 
  // which defines the closest thing to FP in Scala's Try that I'm aware of in scalaz
  def parse(s: String): Option[Int] =
    Maybe.fromTryCatchNonFatal(Integer.parseInt(s)).toOption

  assert(List("21", "42", "33").traverse(parse) == Some(List(21, 42, 33)))

  assert(List("21", "bob", "33").traverse(parse) == None)
}
