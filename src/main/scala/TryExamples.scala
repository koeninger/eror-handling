import scala.util.{ Try, Success, Failure }

object TryExamples {
  def good: Try[String] = Try("ok")
  def bad: Try[String] = Try(throw new Exception("bad"))
  def ugly: Try[String] = Success(throw new Exception("ugly"))

  for {
    g <- good
    u <- ugly
    b <- bad
  } yield s"$good $bad $ugly" 

  for {
    u <- ugly
    g <- good
    b <- bad
  } yield s"$good $bad $ugly" 



  /* definition of Success.flatMap is
  
  def flatMap[U](f: T => Try[U]): Try[U] =
    try f(value)
    catch {
      case NonFatal(e) => Failure(e)
    }
  
  */
}
